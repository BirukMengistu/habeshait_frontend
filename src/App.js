import React from 'react';
import About  from "./components/about";
import Home  from "./components/Home";
import Contact  from "./components/Contact";
import Navbar from './components/Layout/Navbar'; 
import {BrowserRouter as Router , Link, Route, NavLink} from 'react-router-dom'
function App() {
  return (
    <Router>
    <div className="App">
     
        <Navbar />
        <About/>
    </div>
    </Router>
  );
}

export default App;

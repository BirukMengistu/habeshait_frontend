import React from 'react';
import {Link} from 'react-router-dom'
import SignedinLink from './SignedinLink'
import SignedoutLink from './SignedoutLink'
const Navbar =()=>{

    return(
        <nav className="nav-wrapper grey darken-1">
            <div className="conatiner">
             <Link to='/' className="brand-logo">
               Habesha IT
             </Link>
             <SignedinLink/>
             <SignedoutLink/>
            </div>
        </nav>
    )

}
export default Navbar;
import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                <h2 className='blue-text'>About</h2>
                <hr className='blue bold'/>
                <div className="container">
                    <div>
                    <p className='flow-text'>Habesha IT Solution group is founded by ambitious computer science professionals with the aim of providing consultancy and technical support for individuals and small to medium-sized businesses. We are based in Sweden stockholm and have a combined 10+ years of broad experience in software development, 
                    system administration and technical support areas.
                     Our goal is to become a bridge between technology and the different IT related challenges our clients/communities face on a daily basis. We aim to provide assistance and guidance to help our clients to compete and grow in today’s high-tech environment. </p>
                    <button className="btn green white-text">Read more</button>
                    </div>
                </div>
            </div>
        )
    }
}

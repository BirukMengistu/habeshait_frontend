import React, { Component } from 'react'
import { connect } from 'react-redux';
import todo from '../todo.png'
 class Home extends Component {
    render() {
        
        const {posts} = this.props
        const postlist = posts.length?(
            posts.map(post=>{
                return(
                    <div className="post card" key={post.id}>
                        
                    <div className="card-content">
                        
                     { post.id} <span className="card-title">
                      {post.title}   </span>
                    
                    
                   <p>{post.body}</p>   
                    
                    
                    </div>
                    </div>
                )
            })):(
                <div className="center">
                    no posts yet
                </div>

        )

        return (
            <div>
            <div className="jumbotron">
            <h1 className="display-4">Hello, Wellcome Home!</h1>
            <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr className="my-4"/>
            <p>It uses utility classNamees for typography and spacing to space content out within the larger container.</p>
            <p className="lead">
                <a className="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
            </p>
             </div>
             <div className="container home">
             <h4 className="center">Home</h4>
             {postlist}
             </div>
             <a href="#" className="btn-floating red btn-large"><i className="material-icons center">add</i>

             </a>
             
             </div>
        )
    }
}
const mapStateToProps=(state)=>
{
return{
    posts: state.posts
}
}
export default connect(mapStateToProps)(Home)
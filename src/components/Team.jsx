import React, { Component } from 'react'
import team1_bs from '../team_BS.jpg'

export default class Team extends Component {
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col s12 m6">
                    <div className="card">
                        <div className="card-image circle responsive-img ">
                        <img circle responsive-img src={team1_bs} alt= 'Bisrat Girma'/>
                        <span className="card-title">CEO</span>
                        <a className="btn-floating halfway-fab waves-effect waves-light red"><i className="material-icons">add</i></a>
                        </div>
                        <div className="card-content">
                        <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                        </div>
                    </div>
                    </div>
                </div>



                
            </div>
        )
    }
}
